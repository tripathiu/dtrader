from dt.Instrument import Instrument
from dt.Jobber import Jobber
from datetime import datetime
from Utils import Chrono
import os
import json
from os import path


def login():
    return None


class Librarian:
    def __init__(self):
        self.__brok = None
        self.__inst = None
        self.__job = None

    def getCandleData(self, instmnt: str, date: datetime):
        filename = "cache/cdlminute" + str(date.year) + str(date.month).zfill(2) + \
            str(date.day).zfill(2) + instmnt + ".json"

        if os.path.exists(filename):
            print(f"using {filename}")
            with open(filename) as f:
                data = json.load(f)
        else:
            dt = datetime(date.year, date.month, date.day, 15, 15)
            if self.__brok == None:
                self.__brok = login()
            if self.__inst == None:
                self.__inst = Instrument(instmnt, self.__brok)
            if self.__job == None:
                self.__job = Jobber(self.__inst, self.__brok)
            data = self.__job.getCandleData(Chrono.OneMin, 360, dt)
            with open(filename, "w") as f:
                def myconverter(o):
                    if isinstance(o, datetime):
                        return o.__str__()
                json.dump(data, f, default=myconverter)
            with open(filename, "r") as f:
                data = json.load(f)
        return data
