from smartapi import SmartConnect, smartExceptions
import os
from dorktrader import AbstractBroker
import time


class AngelBroking(AbstractBroker):
    def __init__(self, userid=None, passwd=None, api_key=None):

        userid = os.environ["AngelUser"] if not userid else userid
        passwd = os.environ["AngelPass"] if not passwd else passwd
        api_key = os.environ["AngelApiKey"] if not api_key else api_key

        self.brok = SmartConnect(api_key=api_key)
        self.sess = self.brok.generateSession(userid, passwd)
        self.userid = userid

        print(f"Connected to broker using {userid = }")

    def terminate(self):
        try:
            logout = self.brok.terminateSession(self.userid)
            print("Logout Successfull")
        except Exception as e:
            print(f"Logout failed: {e}")

    def placeOrder(self, tranx, inst, qtty, price=None):
        orderparams = {
            "variety": "NORMAL",
            "tradingsymbol": inst.symbol,
            "symboltoken": inst.token,
            "transactiontype": tranx.upper(),
            "exchange": inst.exchange,
            "ordertype": "MARKET" if price == None else "LIMIT",
            "producttype": "INTRADAY",  # TODO: delivery and NRML orders
            "duration": "DAY",  # Order applicatble for the day
            "price": price if price else "0",
            "quantity": qtty,
        }
        orderid = self.brok.placeOrder(orderparams)
        orderparams["orderid"] = orderid
        print("--- Order ---\n")
        for k, v in orderparams.items():
            print(f"  {k}= {v}")
        print("-------------\n")
        return orderparams

    def placeSLOrder(self, tranx, inst, qtty, trigger, price=None):
        if price and tranx.upper() == "BUY" and trigger > price:
            print("Trigger price should be lower than order price for buySL")

        if price and tranx.upper() == "SELL" and trigger < price:
            print("Trigger price should be higher than order price for sellSL")

        orderparams = {
            "variety": "STOPLOSS",
            "tradingsymbol": inst.symbol,
            "symboltoken": inst.token,
            "transactiontype": tranx.upper(),
            "exchange": inst.exchange,
            "ordertype": "STOPLOSS_LIMIT" if price else "STOPLOSS_MARKET",
            "producttype": "INTRADAY",  # TODO: delivery and NRML orders
            "duration": "DAY",  # Order applicatble for the day
            "price": price if price else "0",
            "triggerprice": trigger,
            "quantity": qtty,
        }
        orderid = self.brok.placeOrder(orderparams)
        orderparams["orderid"] = orderid
        print("-- SLOrder --\n")
        for k, v in orderparams.items():
            print(f"  {k}= {v}")
        print("-------------\n")
        return orderparams

    def buy(self, inst, qtty, price=None):
        return self.placeOrder("BUY", inst, qtty, price)

    def sell(self, inst, qtty, price=None):
        return self.placeOrder("SELL", inst, qtty, price)

    def buySL(self, inst, qtty, trigger, price=None):
        return self.placeSLOrder("BUY", inst, qtty, trigger, price)

    def sellSL(self, inst, qtty, trigger, price=None):
        return self.placeSLOrder("SELL", inst, qtty, trigger, price)

    def getQuote(self, instr):
        resp = self.brok.ltpData(instr.exchange, instr.symbol, instr.token)
        if resp["status"] == True:
            return float(resp["data"]["ltp"])

    def modifyOrder(self, orderparams, ordertype=None, price=None, trigger=None):
        if ordertype:
            if ordertype in ["LIMIT", "MARKET"]:
                orderparams["ordertype"] = ordertype
                orderparams["variety"] = "NORMAL"
            elif ordertype in ["STOPLOSS_LIMIT", "STOPLOSS_MARKET"]:
                orderparams["ordertype"] = ordertype
                orderparams["variety"] = "STOPLOSS"
        if price and orderparams["ordertype"] in ["LIMIT", "STOPLOSS_LIMIT"]:
            orderparams["price"] = price
        if trigger and ordertype in ["STOPLOSS_LIMIT", "STOPLOSS_MARKET"]:
            orderparams["triggerprice"] = trigger
        resp = self.brok.modifyOrder(orderparams)
        print(resp["message"])
        return orderparams

    def cancelOrder(self, orderparams):
        resp = self.brok.cancelOrder(orderparams["orderid"], orderparams["variety"])
        return resp

    def getOrderStatus(self, orderparams):
        numAttempts = 5
        while numAttempts > 0:
            try:
                orderbook = self.brok.orderBook()
                break
            except smartExceptions.DataException as e:
                print("rate limit while receiving order book")
                time.sleep(1 + (5 - numAttempts) / 10)
                numAttempts -= 1
        orderid = orderparams["orderid"]
        for order in orderbook["data"]:
            if order["orderid"] == orderid:
                return order["orderstatus"]
