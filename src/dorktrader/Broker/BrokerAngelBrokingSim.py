import os
from dorktrader import AbstractBroker


class AngelBroking(AbstractBroker):
    def __init__(self, userid=None, passwd=None, api_key=None):
        self.simPrice = 100

    def terminate(self):
        pass

    def placeOrder(self, tranx, inst, qtty, price=None):
        orderparams = {
            "variety": "NORMAL",
            "tradingsymbol": inst.symbol,
            "symboltoken": inst.token,
            "transactiontype": tranx.upper(),
            "exchange": inst.exchange,
            "ordertype": "MARKET" if price == None else "LIMIT",
            "producttype": "INTRADAY",  # TODO: delivery and NRML orders
            "duration": "DAY",  # Order applicatble for the day
            "price": price if price else "0",
            "quantity": qtty,
        }
        orderparams["orderid"] = 0
        print("--- ORDER ---")
        for k, v in orderparams.items():
            print(f"  {k}= {v}")
        return orderparams

    def placeSLOrder(self, tranx, inst, qtty, trigger, price=None):
        if price and tranx.upper() == "BUY" and trigger > price:
            print("Trigger price should be lower than order price for buySL")

        if price and tranx.upper() == "SELL" and trigger < price:
            print("Trigger price should be higher than order price for sellSL")

        orderparams = {
            "variety": "STOPLOSS",
            "tradingsymbol": inst.symbol,
            "symboltoken": inst.token,
            "transactiontype": tranx.upper(),
            "exchange": inst.exchange,
            "ordertype": "STOPLOSS_LIMIT" if price else "STOPLOSS_MARKET",
            "producttype": "INTRADAY",  # TODO: delivery and NRML orders
            "duration": "DAY",  # Order applicatble for the day
            "price": price if price else "0",
            "triggerprice": trigger,
            "quantity": qtty,
        }
        orderparams["orderid"] = 0
        print("--- ORDER ---")
        for k, v in orderparams.items():
            print(f"  {k}= {v}")
        return orderparams

    def buy(self, inst, qtty, price=None):
        return self.placeOrder("BUY", inst, qtty, price)

    def sell(self, inst, qtty, price=None):
        return self.placeOrder("SELL", inst, qtty, price)

    def buySL(self, inst, qtty, trigger, price=None):
        return self.placeSLOrder("BUY", inst, qtty, trigger, price)

    def sellSL(self, inst, qtty, trigger, price=None):
        return self.placeSLOrder("SELL", inst, qtty, trigger, price)

    def getQuote(self, instr):
        self.simPrice -= 1
        return self.simPrice

    def modifyOrder(self, orderparams, ordertype=None, price=None, trigger=None):
        if ordertype:
            if ordertype in ["LIMIT", "MARKET"]:
                orderparams["ordertype"] = ordertype
                orderparams["variety"] = "NORMAL"
            elif ordertype in ["STOPLOSS_LIMIT", "STOPLOSS_MARKET"]:
                orderparams["ordertype"] = ordertype
                orderparams["variety"] = "STOPLOSS"
        if price and orderparams["ordertype"] in ["LIMIT", "STOPLOSS_LIMIT"]:
            orderparams["price"] = price
        if trigger and ordertype in ["STOPLOSS_LIMIT", "STOPLOSS_MARKET"]:
            orderparams["triggerprice"] = trigger
        return "ok"

    def cancelOrder(self, orderparams):
        return "ok"

    def getOrderStatus(self, orderparams):
        msg = "complete"
        if orderparams["ordertype"] == "STOPLOSS_LIMIT":
            msg = "open" if self.simPrice != 84 else "complete"
        return msg
