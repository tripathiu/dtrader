import pandas as pd
from Broker import Broker, OrderParams


class HistBroker(Broker):
    def __init__(self, filename):
        self.pdata = pd.read_csv(filename, header=None)
        self.pdata.columns = ['t', 'o', 'h', 'l', 'c', 'vol']
        self.step = 0
        self.holdings = []

    @property
    def ltp(self):
        return self.pdata['c'][self.step]

    def fetchQuote(self, *nargs, **kvargs):
        try:
            close = self.pdata['c'][self.step]
            self.step += 1
        except KeyError:
            close = self.pdata['c'][self.step - 1]
        return close

    def placeOrder(self, params: OrderParams) -> int:
        ltp = self.ltp
        qtty = int(params.quantity)
        side = params.transactiontype.lower()
        for _ in range(qtty):
            self.holdings.append(ltp * (-1 if side == "buy" else 1))
        return 0

    def modifyOrder(self, params: OrderParams):
        pass

    def cancelOrder(self, params: OrderParams):
        pass

    def position(self):
        return sum(self.holdings)


if __name__ == "__main__":
    for g in range(1, 13):
        asset = f"data/sample/AXISBANK-EQ-2020-{g:02}.csv"
        h = HistBroker(asset)
        for i in range(1000):
            h.fetchQuote()
            if i % 20 == 0:
                if i % 40 == 0:
                    prm = OrderParams(quantity=10, transactiontype="BUY")
                else:
                    prm = OrderParams(quantity=10, transactiontype="SELL")
                h.placeOrder(prm)
        print(asset, "\tpl=", h.position())
