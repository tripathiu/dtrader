#! python3

import json
import os.path
import time

from urllib.request import urlretrieve


class InstrumentNotFound(Exception):
    """
    Instrument is not found in the scrip list
    """

    pass


scripFile = f"{os.path.realpath(os.path.dirname(__file__))}/angelscrips.json"
instDatabase = "https://margincalculator.angelbroking.com/OpenAPI_File/files/OpenAPIScripMaster.json"

if not os.path.exists(scripFile):
    print("downloading latest scrips...", end=" ", flush=True)
    urlretrieve(instDatabase, scripFile)
    print("done")

elif time.time() - os.path.getmtime(scripFile) > 6 * 60 * 60:  # six hours old file
    print("scrip file too old, downloading latest scrips...", end=" ", flush=True)
    urlretrieve(instDatabase, scripFile)
    print("done")

with open(scripFile, encoding="utf8") as f:
    print("cacheing scrips...", end=" ", flush=True)
    scrips = json.load(f)
    if scrips:
        print("done")


class Instrument:
    def __init__(self, symbol, exch):
        filtFunc = lambda x: x["symbol"] == symbol and x["exch_seg"] == exch
        instList = list(filter(filtFunc, scrips))
        if not instList:
            raise InstrumentNotFound
        if len(instList) > 1:
            print(f"Multiple instruments found: {instList}. Selecting {instList[0]}")
        self.__inst = instList[0]
        print(f"Created: ")
        self.showDetails()

    @classmethod
    def Option(cls, name, expiry, strike, opttype):
        def filtFunct(item):
            cond1 = item["name"] == name.upper()
            cond2 = item["expiry"] == expiry.upper()
            cond3 = int(float(item["strike"]) / 100) == strike
            cond4 = item["symbol"][-2:] == opttype.upper()
            return cond1 and cond2 and cond3 and cond4

        instList = list(filter(filtFunct, scrips))
        if not instList:
            raise InstrumentNotFound
        if len(instList) > 1:
            print(f"Multiple instruments found: {instList}. Selecting {instList[0]}")
        inst = instList[0]
        obj = Instrument(inst["symbol"], inst["exch_seg"])
        return obj

    def showDetails(self):
        print(
            f"""Token: {self.token}\nSymbl: {self.symbol}\nScrip: {self.name}\nExchg: {self.exchange}"""
        )

    @property
    def token(self) -> str:
        return self.__inst["token"]

    @property
    def name(self) -> str:
        return self.__inst["name"]

    @property
    def symbol(self) -> str:
        return self.__inst["symbol"]

    @property
    def exchange(self) -> str:
        return self.__inst["exch_seg"]
