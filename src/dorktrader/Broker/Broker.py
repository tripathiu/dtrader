class AbstractBroker:
    def placeOrder(self, tranx, instr, qtty, price=None):
        """
        Sends a place order requrest to the broker.
        tranx is either 'BUY' or 'SELL'
        instrument could be a stock, future or option
        if price is specified, place a limit order
        """
        raise NotImplementedError

    def placeSLOrder(self, tranx, instr, qtty, trigger, price=None):
        """
        Sends a place stoploss order to the broker type is either 'BUY' or
        'SELL'. Note that for bullish positions, the stoploss has to be on the
        sell side and for bearish positions, stoploss has to be buy.
        """
        raise NotImplementedError

    def modifyOrder(self, orderId, price=None, trigger=None):
        """
        Modify the price and or the trigger price for the given order
        """
        raise NotImplementedError

    def cancelOrder(self, id):
        """
        Cancel the order for the given order id
        """
        raise NotImplementedError

    def getOrderStatus(self, id):
        """
        check if the oreder is executed or not
        """
        raise NotImplementedError

    def getQuote(self, instr):
        """
        Fetch the current price of the instrument
        """
        raise NotImplementedError
