#! /usr/bin/python3

from datetime import datetime, timedelta, time

def strToTime(string: str) -> datetime:
    d = str(string).split()[0].split("-")
    t = str(string).split()[1].split(":")
    # dt = datetime(int(d[0]), int(d[1]), int(d[2]), int(t[0]), int(t[1]))
    dt = time(int(t[0]), int(t[1]))
    return dt

class Position:
    def __init__(self):
        self.orders = [{"qtty": 0, "price": 0}]

    def quantity(self):
        return sum([q['qtty'] for q in self.orders])

    def avgbuyprice(self):
        buys = [q for q in self.orders if q['qtty'] > 0]
        return Position.avgprice(buys)

    def avgsellprice(self):
        buys = [q for q in self.orders if q['qtty'] < 0]
        return Position.avgprice(buys)

    def add(self, qtty: int, price: float):
        self.orders.append({'qtty': qtty, 'price': price})

    def exit(self, price: float):
        self.add(-1 * self.quantity(), price)

    def pl(self, currprice: float):
        pllist = [0]
        for o in self.orders:
            pllist.append(o['qtty']*currprice - o['qtty']*o['price'])
        return sum(pllist)

    @staticmethod
    def avgprice(orders):
        totalqtty = sum([p['qtty'] for p in orders])
        if totalqtty == 0:
            return 0
        return sum([p['price']*p['qtty'] for p in orders]) / totalqtty


class Chrono:
    OneMin = "minute"
    ThreeMin = "3minute"
    FiveMin = "5minute"
    TenMin = "10minute"
    FifteenMin = "15minute"
    ThirtyMin = "30minute"
    OneHour = "60minute"
    OneDay = "day"

    TimeFmt = "%Y-%m-%d %H:%M"

    @ staticmethod
    def now() -> str:
        return datetime.now().strftime(Chrono.TimeFmt)

    @ staticmethod
    def formatTime(dt: datetime):
        return dt.strftime(Chrono.TimeFmt)

    @ staticmethod
    def minutesBack(min_: int, dt) -> str:
        delta = timedelta(minutes=min_)
        time = dt - delta
        mktStartTime = datetime(dt.year, dt.month, dt.day, 9, 15)
        if time < mktStartTime:
            prevMktDelta = timedelta(seconds=63840)
            time = time - prevMktDelta
        return time.strftime(Chrono.TimeFmt)
