#! python3

from datetime import datetime
from dt.Instrument import Instrument
from dt.Broker import Broker, OrderParams


class Quote:
    def __init__(self, low, hi, opn, clos, ltp):
        self.low = low
        self.high = hi
        self.open = opn
        self.close = clos
        self.ltp = ltp


class OrderFailed(Exception):
    pass


class LtpDataResponseFailed(Exception):
    pass


class Jobber:
    def __init__(self, instrument: Instrument, broker: Broker):
        self.__inst = instrument
        self.__brok = broker
        print("JOB: Jobber created for ", self.__inst.symbol)

    def inst(self):
        return self.__inst

    def getQuote(self):
        return self.__brok.fetchQuote(self.__inst.exchange, self.__inst.symbol, self.__inst.token)

    def buyMkt(self, quantity):
        return self.__orderMkt(quantity, "BUY")

    def sellMkt(self, quantity):
        return self.__orderMkt(quantity, "SELL")

    def __order(self, quantity, type):
        p = OrderParams(
            variety="NORMAL",
            tradingsymbol=self.__inst.symbol,
            symboltoken=self.__inst.token,
            transactiontype=type,
            exchange=self.__inst.exchange,
            ordertype="MARKET",
            producttype="INTRADAY",
            duration="DAY",
            quantity=quantity)
        orderid = self.__brok.placeOrder(p)
        return orderid

    def getCandles(self, interv: str, fmTime, toTime):
        historicParam = {
            "exchange": "NSE",
            "symboltoken": ,
            "interval": "ONE_MINUTE",
            "fromdate": fmTime,
            "todate": toTime
        }
        return self.__brok.getCandleData(historicParam)
