#! /usr/bin/python3

import talib
import pandas as pd
import math


class NotEnoughData(Exception):
    pass


class ATRTSL:
    def __init__(self, pd_: int = 14, mult_: float = 3.0):
        self.__slShort = 0
        self.__slLong = 0
        self.__pos = ""
        self.__pd = pd_
        self.__mult = mult_
        self.mult = mult_
        self.__tradedPrice = 0

    def step(self, cdlData: pd.DataFrame) -> str:
        df = pd.DataFrame(cdlData)
        T, O, H, L, C = df['date'].values, df['open'].values, df['high'].values, df['low'].values, df['close'].values

        atrVec1 = talib.ATR(H, L, C, self.__pd) * self.__mult
        atrVec2 = talib.ATR(H, L, C, int(self.__pd/2)) * self.__mult

        if math.isnan(atrVec1[-1]):
            raise NotEnoughData

        newAtrSl = atrVec1[-1]  # + atrVec2[-1])/2

        signal = "Hold"

        if self.__pos == "":  # not decided yet position
            self.__slShort = C[-1] + newAtrSl
            self.__slLong = C[-1] - newAtrSl
            self.__pos = "Wait"

        elif self.__pos == "Short":  # not decided yet position
            if C[-1] > self.__slShort:
                self.__slLong = max(C[-1], O[-1]) - newAtrSl
                self.__pos = "Long"
                signal = "Buy"
            elif C[-2] > C[-1]:
                self.__slShort = self.__slShort - (C[-2] - C[-1])

        elif self.__pos == "Long":  # not decided yet position
            if C[-1] < self.__slLong:  # breakthrough
                self.__slShort = min(C[-1], O[-1]) + newAtrSl
                self.__pos = "Short"
                signal = "Sell"
            elif C[-2] < C[-1]:
                self.__slLong = self.__slLong + C[-1] - C[-2]

        elif self.__pos == "Wait":  # Check for breakthroughs
            if C[-1] > self.__slShort:  # breakthrough
                self.__pos = "Long"
                self.__slLong = C[-1] - newAtrSl
                signal = "Buy"

            elif C[-1] < self.__slLong:  # breakthrough
                self.__pos = "Short"
                self.__slShort = C[-1] + newAtrSl
                signal = "Sell"

            # otherwise update stoplosses
            elif C[-2] < C[-1]:
                self.__slLong = self.__slLong + C[-1] - C[-2]
            elif C[-2] > C[-1]:
                self.__slShort = self.__slShort - (C[-2] - C[-1])

        if signal == "Buy":
            print(f"B at {C[-1]} at {T[-1]} at PL 0")
        elif signal == "Sell":
            print(f"S at {C[-1]} at {T[-1]} at PL 0")
        sl = self.__slShort
        if self.__pos == "Long":
            sl = self.__slLong
        self.__tradedPrice = C[-1]
        return signal, self.__slShort, self.__slLong, sl, C[-1]
