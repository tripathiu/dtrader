#! /usr/bin/python3

import talib
import pandas as pd
import math
from TrailingStoploss import TrailingStoploss as Tsl


class NotEnoughData(Exception):
    pass


class ATRTSL:
    def __init__(self, pd_: int = 14, mult_: float = 3.0):
        self.__slShort = 0
        self.__slLong = 0
        self.__pos = ""
        self.__pd = pd_
        self.__mult = mult_
        self.mult = mult_
        self.__tradedPrice = 0

    def step(self, cdlData: pd.DataFrame) -> str:
        df = pd.DataFrame(cdlData)
        T, O, H, L, C = df['date'].values, df['open'].values, df['high'].values, df['low'].values, df['close'].values

        atrVec1 = talib.ATR(H, L, C, self.__pd) * self.__mult

        if math.isnan(atrVec1[-1]):
            raise NotEnoughData

        newAtrSl = atrVec1[-1]  # + 3*atrVec2[-1])/4
        mom = talib.MOM(df['close'].values, self.__pd/2)[-1]
        rsi1 = talib.RSI(df['close'].values, self.__pd)[-1]
        rsi2 = talib.RSI(df['close'].values, int(self.__pd/3))[-1]
        sma = talib.SMA(df['close'].values, self.__pd/2)[-1]

        signal = "Hold"

        if self.__pos == "":  # not decided yet position
            self.__slShort = Tsl(C[-1], newAtrSl, "Short")
            self.__slLong = Tsl(C[-1], newAtrSl, "Long")
            self.__pos = "Wait"

        elif self.__pos == "Short":  # not decided yet position
            self.__slShort.step(min(C[-1], O[-1]))
            if rsi1 < 30 and rsi2 < 30 or self.__slShort.isBroken():
                self.__slLong = Tsl(max(C[-1], O[-1]), newAtrSl, "Long")
                self.__pos = "Long"
                signal = "Buy"

        elif self.__pos == "Long":  # not decided yet position
            self.__slLong.step(max(C[-1], O[-1]))
            if rsi1 > 70 and rsi2 > 70 or self.__slLong.isBroken():
                self.__slShort = Tsl(min(C[-1], O[-1]), newAtrSl, "Short")
                self.__pos = "Short"
                signal = "Sell"

        elif self.__pos == "Wait":  # Check for breakthroughs
            self.__slLong.step(C[-1])
            self.__slShort.step(C[-1])

            if self.__slShort.isBroken():
                self.__pos = "Long"
                self.__slLong = Tsl(C[-1], newAtrSl, "Long")
                signal = "Buy"

            elif self.__slLong.isBroken():
                self.__pos = "Short"
                self.__slShort = Tsl(C[-1], newAtrSl, "Short")
                signal = "Sell"

        if signal == "Buy":
            print(f"B at {C[-1]} at {T[-1]} at PL 0")
        elif signal == "Sell":
            print(f"S at {C[-1]} at {T[-1]} at PL 0")
        sl = self.__slShort.stoploss()
        if self.__pos == "Long":
            sl = self.__slLong.stoploss()
        self.__tradedPrice = C[-1]
        return signal, self.__slShort.stoploss(), self.__slLong.stoploss(), sl, C[-1]
