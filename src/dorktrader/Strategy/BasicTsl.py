class TrailingStoploss:
    TypeShort = "Short"
    TypeLong = "Long"

    def __init__(self, tradePrice, stoplossDelta, slType):
        self.__type = slType
        self.__tradePrice = tradePrice
        self.setDelta(stoplossDelta)
        self.__stoploss = self.__tradePrice - self.__stoplossDelta

    def step(self, newPrice):
        slD = self.__stoplossDelta + self.__stoploss
        typ = self.__type
        if (newPrice > slD and typ == "Long") or (newPrice < slD and typ == "Short"):
            delta = newPrice - (self.__stoploss + self.__stoplossDelta)
            self.__stoploss = self.__stoploss + delta
        self.__tradePrice = newPrice
        return self.isBroken()

    def setDelta(self, stoplossDelta):
        if self.__type == TrailingStoploss.TypeLong:
            self.__stoplossDelta = stoplossDelta
        elif self.__type == TrailingStoploss.TypeShort:
            self.__stoplossDelta = -stoplossDelta

    def delta(self):
        return self.__stoplossDelta

    def stoploss(self):
        return self.__stoploss

    def isBreached(self):
        price = self.__tradePrice
        if self.__type == TrailingStoploss.TypeShort:
            if price > self.__stoploss:
                return True
        elif self.__type == TrailingStoploss.TypeLong:
            if price < self.__stoploss:
                return True
        return False
