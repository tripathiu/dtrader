#! /usr/bin/python3

from datetime import time
import talib
import pandas as pd
import math
from Utils import TrailingStoploss as Tsl
from Utils import Signal, Position, strToTime
from enum import Enum


class NotEnoughData(Exception):
    pass


class Strategy:
    def __init__(self, dayTgt=0.2, trdTgt=0.7, slMult=2.1):
        self.pos = Position.Setup
        self.waitNum = 0

        self.T = None
        self.O = None
        self.H = None
        self.L = None
        self.C = None

        self.slLong = None
        self.slShort = None

        self.traded = 0
        self.target = 0.2

        self.pl = 0
        self.tradeplTarget = 0

        self.slMult = slMult
        self.dayTgt = dayTgt
        self.trdTgt = trdTgt
        self.numTrade = 0

    # LONG
    def longEntryCond(self) -> bool:
        cond = self.slShort.isBroken()
        return cond

    def longEntryOps(self):
        self.slLong = Tsl(self.C[-1], self.slMult*self.atr[-1], Tsl.TypeLong)
        self.traded = self.C[-1]
        self.pos = Position.Long

    def longExitCond(self) -> bool:
        cond1 = self.slLong.isBroken()
        cond2 = ((self.C[-1] - self.traded) * 100 / self.traded) > self.trdTgt
        # cond3 = self.currTime > time(15, 10)
        return cond1 or cond2  # or cond3

    def longExitOps(self):
        pl = ((self.C[-1] - self.traded) * 100 / self.traded)
        self.pl = self.pl + pl
        self.pos = Position.Short
        if pl > self.trdTgt:
            self.setupOps()
            self.pos = Position.Wait
        if self.pl > self.dayTgt:  # or self.currTime > time(15, 10):
            self.pos = Position.Halt
        self.numTrade = self.numTrade + 1
        if self.numTrade >= 6:
            self.dayTgt = self.dayTgt*0.8
        if self.numTrade >= 12:
            self.dayTgt = -0.3
            if self.numTrade == 12:
                self.trdTgt = self.trdTgt*0.1

    # SHORT
    def shortEntryCond(self) -> bool:
        cond = self.slLong.isBroken()
        return cond

    def shortEntryOps(self):
        self.slShort = Tsl(self.C[-1], self.slMult*self.atr[-1], Tsl.TypeShort)
        self.traded = self.C[-1]
        self.pos = Position.Short

    def shortExitCond(self) -> bool:
        cond1 = self.slShort.isBroken()
        cond2 = ((self.traded - self.C[-1]) * 100 / self.traded) > self.trdTgt
        # cond3 = self.currTime > time(15, 10)
        return cond1 or cond2

    def shortExitOps(self):
        pl = ((self.traded - self.C[-1]) * 100 / self.traded)
        self.pl = self.pl + pl
        self.pos = Position.Long
        if pl > self.trdTgt:
            self.setupOps()
            self.pos = Position.Wait
        if self.pl > self.dayTgt:  # or self.currTime > time(15, 10):
            self.pos = Position.Halt
        self.numTrade = self.numTrade + 1
        if self.numTrade >= 6:
            self.dayTgt = self.dayTgt*0.8
        if self.numTrade >= 12:
            self.dayTgt = -0.3
            if self.numTrade == 12:
                self.trdTgt = self.trdTgt*0.1

    def setupOps(self):
        if self.waitNum == 0:
            self.slLong = Tsl(self.C[-1], self.slMult *
                              self.atr[-1], Tsl.TypeLong)
            self.slShort = Tsl(
                self.C[-1], self.slMult*self.atr[-1], Tsl.TypeShort)
            self.pos = Position.Wait

    def everyStep(self, arg):
        try:
            self.atr = talib.ATR(self.H, self.L, self.C, 18)
            # self.currTime = strToTime(self.T[-1])
            if self.T.size == 0:
                raise NotEnoughData
            if self.waitNum > 0:
                self.waitNum = self.waitNum - 1
            if self.slLong != None:
                self.slLong.step(self.C[-1])
                # self.slLong.setDelta(self.slLong.delta() * 0.95)
            if self.slShort != None:
                self.slShort.step(self.C[-1])
                # self.slShort.setDelta(self.slShort.delta() * 0.95)
        except Exception as e:
            print(f"Error: {e}")

    def step(self, cdlData: pd.DataFrame) -> Signal:
        df = pd.DataFrame(cdlData)
        self.T = df['date'].values
        self.O = df['open'].values
        self.H = df['high'].values
        self.L = df['low'].values
        self.C = df['close'].values

        signal = Signal.Hold

        self.everyStep(df)

        # SETUP
        if self.pos == Position.Setup:
            self.setupOps()

        # WAIT FOR OPPORTUNITY
        elif self.pos == Position.Wait:
            # ENTRY LONG
            if self.longEntryCond():
                self.longEntryOps()
                signal = Signal.Buy

            # ENTRY SHORT
            elif self.shortEntryCond():
                self.shortEntryOps()
                signal = Signal.Sell

        # EXIT LONG
        elif self.pos == Position.Long:
            if self.longExitCond():
                self.longExitOps()
                signal = Signal.Sell
                if self.pos == Position.Short:  # reversal
                    self.shortEntryOps()
                    signal = Signal.DSell

        # EXIT SHORT
        elif self.pos == Position.Short:
            if self.shortExitCond():
                self.shortExitOps()
                signal = Signal.Buy
                if self.pos == Position.Long:  # reversal
                    self.longEntryOps()
                    signal = Signal.DBuy

        elif self.pos == Position.Halt:
            signal = signal.Hold

        try:
            sl1 = self.C[-1]
            sl2 = self.C[-1]
            C = self.C[-1]
            T = self.T[-1]
        except Exception as e:
            print(f"Error {e}")
            sl1 = self.traded
            sl2 = self.traded
            C = self.traded
            T = self.traded
        if self.slLong != None:
            sl1 = self.slLong.stoploss()

        if self.slShort != None:
            sl2 = self.slShort.stoploss()

        return signal, C  # ,  str(T), sl1, sl2
