class Node:
    def __init__(self, ident):
        self.parent = None
        self.children = []
        self.ident = ident
        self.isTriggered = False

    def showTree(self, depth=0):
        print(f"{'  '*depth}|-{self.ident} Trig:{self.isTriggered}")
        depth += 1
        for child in self.children:
            child.showTree(depth)


class Strategy(Node):
    def __init__(self, ident: str):
        super().__init__(ident)
        self.price = None

    def tick(self, newPrice):
        self.price = newPrice
        if self.entryCond() or self.exitCond():
            self.isTriggered = True
            self.parent.trigger()

    def exitCond(self) -> bool:
        raise NotImplementedError(
            f"Derived class should implement {__name__} method")

    def entryCond(self) -> bool:
        raise NotImplementedError(
            f"Derived class should implement {__name__} method")


class StrategyContainer(Node):
    def __init__(self, ident: str):
        super().__init__(ident)

    def tick(self, ltp):
        for child in self.children:
            child.tick(ltp)

    def trigger(self):
        self.isTriggered = True
        if self.parent:
            self.parent.trigger()

    def addChild(self, strategy: Strategy):
        for s in self.children:
            if s.ident == strategy.ident:
                print(
                    f"Strategy or StrategyContainer with the same identifier exists already")
                return
        self.children.append(strategy)
        strategy.parent = self
        return self

    def getTriggeredNodes(self) -> list:
        l = []
        for child in self.children:
            if type(child) == StrategyContainer:
                m = child.getTriggeredNodes()
                for n in m:
                    l.append(n)
            elif child.isTriggered:
                l.append(child)
        return l
