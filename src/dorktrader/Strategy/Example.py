from dt.Strategy.Strategy import Strategy


class Target(Strategy):
    def __init__(self, tgt: int, ident="target 0"):
        self.tgt = tgt
        super().__init__(ident)

    def entryCond(self):
        return False


class LongTarget(Target):
    def __init__(self, tgt: int, ident="target 0"):
        super().__init__(tgt, ident)

    def exitCond(self):
        return self.price >= self.tgt


class LongStoploss(Target):
    def __init__(self, sl: int, ident="target 0"):
        super().__init__(sl, ident)

    def exitCond(self):
        return self.price <= self.tgt
