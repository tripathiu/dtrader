#! python3

# First set the stoploss and entry type and entry quantity
# Then check the ltp, set sell = ltp - stoploss or buy = ltp + stoploss
# Place order
# Step: check the ltp and compare w/ stoploss and update stoploss. Return status
# __del__: square off

class TSL:
    Short = 0
    Long = 1

    def __init__(self, typ, job, delta, qty):
        self.__job = job
        self.__qty = qty
        self.__delta = delta
        self.__type = typ
        ltp = self.__job.getQuote().ltp
        self.__ltp = ltp
        self.__entryLtp = ltp
        self.__isWinning = False
        if typ == TSL.Short:
            self.__tsl = (ltp + delta)
        else:
            self.__tsl = (ltp - delta)
        print(f"TSL: Placing order. Risk {self.__qty * self.__delta}")
        self.__enter()
        print("TSL: Order placed. Run step periodically")
        self.__isComplete = False

    def __del__(self):
        self.__exit()

    # This is the algorithm
    def step(self):
        if self.__isComplete == True:
            return False

        ltp = self.__job.getQuote().ltp
        if ltp == 0:
            print("TSL: Invalid ltp")
            return True
        self.__ltp = ltp
        if self.__type == TSL.Long:
            if ltp - self.__delta > self.__tsl:
                self.__tsl = ltp - self.__delta  # update TSL
                if self.__isWinning == False and self.__tsl > self.__entryLtp:
                    self.__isWinning = True
                    print("TSL: This is a winning trade!")

            if ltp <= self.__tsl:
                self.__exit()
                return False
        else:
            if ltp + self.__delta < self.__tsl:
                self.__tsl = ltp + self.__delta  # update TSL
                if self.__isWinning == False and self.__tsl < self.__entryLtp:
                    self.__isWinning = True
                    print("TSL: This is a winning trade!")
            if ltp >= self.__tsl:
                self.__exit()
                return False
        print("TSL: ltp:", ltp, "tsl:", self.__tsl, "pos:", self.pl())
        return True

    def pl(self):
        plPerShare = self.__entryLtp - self.__ltp
        if self.__type == TSL.Short:
            pl = plPerShare * self.__qty
        else:
            pl = -plPerShare * self.__qty
        return pl

    def __enter(self):
        if self.__type == TSL.Short:
            self.__job.sellMkt(self.__qty)
        else:
            self.__job.buyMkt(self.__qty)
        print(f"TSL: Entry at {self.__ltp}, SL = {self.__tsl}")

    def __exit(self):
        if(self.__isComplete == False):
            self.__isComplete = True
            if self.__type == TSL.Short:
                self.__job.buyMkt(self.__qty)
            else:
                self.__job.sellMkt(self.__qty)
            print(f"TSL: Exitting at {self.__ltp}, PL: {self.pl()}")

    def exitTrade(self):
        print("TSL: Manually exitting")
        self.__exit()
