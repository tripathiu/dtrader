#! python3

from kiteLogin import kiteLogin

from Jobber import Jobber, Chrono
from Instrument import Instrument

from datetime import datetime, timedelta
from time import sleep

import plotly.graph_objects as go

import pandas as pd
import talib
from BuySell import ATRTSL as AtrAlgo
from BuySell import NotEnoughData

from CdlData import Librarian

import os

qtty = 45


def simulate(atr, toDt, data):
    df = pd.DataFrame(data)

    data = [go.Candlestick(x=df['date'],
                           open=df['open'],
                           high=df['high'],
                           low=df['low'],
                           close=df['close'])]
    fig = go.Figure(data)

    ant = []
    slShort = []
    slLong = []
    sl = []
    t = []

    buyPrice = []
    sellPrice = []

    cumulativePl = 0
    prevTradePrice = 0

    for i in range(perd+5, 340):
        try:
            sig, sl1, sl2, s, tradePrice = atr.step(df.loc[perd-5:i])
            slShort.append(sl1)
            slLong.append(sl2)
            sl.append(s)
            t.append(df['date'][i])
            if sig != "Hold":
                ornt = 100
                fnt = dict(size=16, color="#00ff00")
                if sig == "Sell":
                    ornt = -100
                    fnt = dict(size=16, color="#ff0000")

                if sig == "Buy":
                    buyPrice.append(tradePrice)
                    if prevTradePrice != 0:
                        cumulativePl = cumulativePl + prevTradePrice - tradePrice
                elif sig == "Sell":
                    sellPrice.append(tradePrice)
                    if prevTradePrice != 0:
                        cumulativePl = cumulativePl + tradePrice - prevTradePrice

                prevTradePrice = tradePrice

                cumlPlStr = "{:.2f}".format(cumulativePl * qtty)

                ant.append(
                    dict(x=df['date'][i-1], y=df['close'][i-1], xref='x', yref='y',
                         showarrow=True, xanchor='left', text=sig + " PL:" + cumlPlStr,
                         font=fnt, ax=0, ay=ornt, arrowhead=1, arrowsize=2,)
                )

        except NotEnoughData:
            print("Holding...")
            continue

    fig.update_layout(annotations=ant)
    fig.add_trace(go.Scatter(
        x=t,
        y=sl,
        mode="lines+markers",
        name="Stoploss",
        visible="legendonly"
    ))
# fig.add_trace(go.Scatter(
#     x=t,
#     y=slLongj,
#     mode="lines+markers",
#     name="Lines and Text",
# ))

    # print(f"B: {sum(buyPrice)/len(buyPrice)} S: {sum(sellPrice)/len(sellPrice)}")

    profitOrLoss = []

    for i in range(min(len(buyPrice), len(sellPrice))-1):
        profitOrLoss.append(sellPrice[i] - buyPrice[i])
        profitOrLoss.append(sellPrice[i] - buyPrice[i+1])

    strFmt = "%Y-%m-%d"

    print(
        f"[RESULT] Date: {toDt.strftime(strFmt)} MULT={atr.mult} NUmTRede= {len(profitOrLoss)} pl={sum(profitOrLoss)}")

    # fig.show()
    if not os.path.exists("./images"):
        os.mkdir("images")

    simname = str(datetime.now())

    fig.write_image("images/"+simname+".png", width=1920, height=1080)


# broker.terminateSession(UserCred.cId)
librarian = Librarian()
inst = "TATASTEEL"
perd = int(input("Period:"))
mult = float(input("Mult:"))
for i in range(1, 31):
    toDt = datetime(2021, 4, i, 15, 15)
    if toDt.weekday() >= 5:
        continue
    data = librarian.getCandleData(inst, toDt)
    if data == []:
        print("holiday on: " + str(toDt))
        continue
    atr = AtrAlgo(perd, mult)
    simulate(atr, toDt, data)

# toDt = datetime(2021, 4, i, 15, 15)
#
# broker = kiteLogin()
# hcl = Instrument("TATASTEEL", broker)
# hclJob = Jobber(hcl, broker)
#
# while True:
#     mult = float(input("Mult:"))
#     perd = int(input("period:"))
#     toDt = datetime(2021, 4, i, 15, 15)
#     data = hclJob.getCandleData(Chrono.OneMin, 360, toDt)
#     atr = AtrAlgo(perd, mult)
#     simulate(atr, toDt, data)
