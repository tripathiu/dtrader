# Darth Trader

> Come join the dark side when entering the markets. Let the force flow through 
  your strategies: take your profits to a galaxy far, far away!

![Darth Trader using the force to control the markets](./docs/graphic.png)

# System Architecture

This image gives a block overview of the planned system architecture.

![](./docs/arch.png)

Green represents the user terminal block, or to connect a frontend

Blue represents the strategy block

Orange represents the broker connectors and historical database classes

# Planned features

* Broker, Jobber, and Instrument creation in the backend

* Client connects to the server using gRPC > Skip to frontend

* Client can create strategies and execute order buying and selling

* Mock broker for backtesting

Any new feature requests are welcome: please don't hesitate to create an issue for the same. 
