import pytest
from dt.Instrument import Instrument, InstrumentNotFound

inst1 = Instrument("TCS")
inst2 = Instrument("INFY")
inst3 = Instrument("AXISBANK", exch="BSE")


def test_symbol():
    assert inst1.symbol == "TCS-EQ"
    assert inst2.symbol == "INFY-EQ"
    assert inst3.symbol == "AXISBANK"


def test_exch():
    assert inst1.exchange == "NSE"
    assert inst2.exchange == "NSE"
    assert inst3.exchange == "BSE"


def test_name():
    assert inst1.name == "TCS"
    assert inst2.name == "INFY"
    assert inst3.name == "AXISBANK"


def test_token():
    assert inst1.token == "11536"
    assert inst2.token == "1594"
    assert inst3.token == "532215"
