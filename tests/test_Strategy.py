from dt.Strategy.Strategy import Strategy, StrategyContainer


def test_constructor():
    c = Strategy("Strat")
    a = StrategyContainer("Cont")
    assert c.ident == "Strat"
    assert a.ident == "Cont"
    assert a.children == []
    assert c.parent == None
    assert a.parent == None


def test_oneLevelTree():
    a = StrategyContainer("L1")
    b = Strategy("L1.1")
    c = Strategy("L1.2")
    a.addChild(b).addChild(c)
    assert a.ident == "L1"
    assert b.ident == "L1.1"
    assert c.ident == "L1.2"

    assert a.children[0] == b
    assert a.children[1] == c


def test_twoLevelTree():
    a = StrategyContainer("L1")
    b = Strategy("L1.1")
    c = Strategy("L1.2")
    d = StrategyContainer("L2")
    e = Strategy("L2.1")
    a.addChild(b).addChild(c).addChild(d)
    d.addChild(e)

    assert a.children[0] == b
    assert a.children[1] == c
    assert a.children[2] == d
    assert len(a.children) == 3
    assert d.children[0] == e
    assert e.parent == d
    assert d.parent == a
    assert len(d.children) == 1


class StayAwayFromTheMarketStrategy(Strategy):
    def exitCond(self) -> bool:
        return False

    def entryCond(self) -> bool:
        return False


def test_priceGoesDown():
    a = StrategyContainer("L1")
    b = StayAwayFromTheMarketStrategy("L1.1")
    c = StayAwayFromTheMarketStrategy("L1.2")
    d = StrategyContainer("L2")
    e = StayAwayFromTheMarketStrategy("L2.1")
    a.addChild(b).addChild(c).addChild(d)
    d.addChild(e)
    a.tick(10)
    assert e.price == 10
    a.tick(20)
    assert b.price == 20
    assert c.price == 20
    assert e.price == 20


class Finicky(Strategy):

    def tick(self, newPrice):
        print(f"{self.ident} ticked with new price {newPrice}")
        return super().tick(newPrice)

    def exitCond(self) -> bool:
        return True if self.price == 15 else False

    def entryCond(self) -> bool:
        return True if self.price == 10 else False


def test_triggersGoUp_AllTriggered():
    a = StrategyContainer("L1")
    b = Finicky("L1.1")
    c = Finicky("L1.2")
    d = StrategyContainer("L2")
    e = Finicky("L2.1")
    a.addChild(b).addChild(c).addChild(d)
    d.addChild(e)

    for i in range(1, 10):
        a.tick(i)

    assert a.isTriggered == False

    a.tick(10)
    assert a.isTriggered == True
    assert b.isTriggered == True
    assert c.isTriggered == True
    assert d.isTriggered == True
    assert e.isTriggered == True

    assert e.entryCond() == True
    assert e.exitCond() == False

    print(a.showTree())
    print(b.price)

    l = a.getTriggeredNodes()
    print(l)
    assert len(l) == 3
    print(f"{l[0].ident} triggered at {10}")
    print(f"{l[1].ident} triggered at {10}")
    print(f"{l[2].ident} triggered at {10}")
    assert b in l
    assert c in l
    assert e in l


def test_triggersGoUp_OneTriggered():
    a = StrategyContainer("L1")
    b = StayAwayFromTheMarketStrategy("L1.1")
    c = StayAwayFromTheMarketStrategy("L1.2")
    d = StrategyContainer("L2")
    e = Finicky("L2.1")
    a.addChild(b).addChild(c).addChild(d)
    d.addChild(e)

    for i in range(1, 10):
        a.tick(i)

    assert a.isTriggered == False

    a.tick(10)
    assert a.isTriggered == True
    assert b.isTriggered == False
    assert c.isTriggered == False
    assert d.isTriggered == True
    assert e.isTriggered == True

    assert e.entryCond() == True
    assert e.exitCond() == False

    print(a.showTree())
    print(b.price)

    l = a.getTriggeredNodes()
    print(l)
    assert len(l) == 1
    print(f"{l[0].ident} triggered at {10}")
    assert not b in l
    assert not c in l
    assert e in l
