from dt.Strategy.Example import *
from dt.Strategy.Strategy import StrategyContainer


def test_LongTgtTrigger():
    myStrategy = StrategyContainer("TgtSl")
    myStrategy.addChild(LongTarget(30, "Target")) \
        .addChild(LongStoploss(10, "Stoploss"))

    myStrategy.showTree()
    for price in range(15, 35):
        myStrategy.tick(price)
        if myStrategy.isTriggered:
            triggered = myStrategy.getTriggeredNodes()
            for node in triggered:
                print(f"{node.ident} triggered! at price {price}")
            break
    assert type(triggered[0]) == LongTarget


def test_LongSLTrigger():
    myStrategy = StrategyContainer("TgtSl")
    myStrategy.addChild(LongTarget(30, "Target")) \
        .addChild(LongStoploss(10, "Stoploss"))

    myStrategy.showTree()
    prices = [15 - x for x in range(15)]
    for price in prices:
        myStrategy.tick(price)
        if myStrategy.isTriggered:
            triggered = myStrategy.getTriggeredNodes()
            for node in triggered:
                print(f"{node.ident} triggered! at price {price}")
            break
    assert type(triggered[0]) == LongStoploss
