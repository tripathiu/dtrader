import pytest
from dt.Utils import Position


def test_constructor():
    p = Position()
    assert p.orders[0]['qtty'] == 0
    assert p.orders[0]['price'] == 0
    assert len(p.orders) == 1


def test_no_position():
    p = Position()
    assert p.quantity() == 0
    assert p.avgbuyprice() == 0
    assert p.avgsellprice() == 0
    assert p.pl(10) == 0
    assert p.pl(20) == 0


def test_avg_buy_side():
    p = Position()
    p.add(10, 10)
    assert p.quantity() == 10
    assert p.avgbuyprice() == 10
    assert p.avgsellprice() == 0
    p.add(10, 20)
    assert p.quantity() == 20
    assert p.avgbuyprice() == 15
    assert p.avgsellprice() == 0
    p.add(-10, 20)
    assert p.quantity() == 10
    assert p.avgbuyprice() == 15
    assert p.avgsellprice() == 20
    p.add(-10, 10)
    assert p.quantity() == 0
    assert p.avgbuyprice() == 15
    assert p.avgsellprice() == 15


def test_avg_sell_side():
    p = Position()
    p.add(-10, 10)
    assert p.quantity() == -10
    assert p.avgbuyprice() == 0
    assert p.avgsellprice() == 10
    p.add(-10, 20)
    assert p.quantity() == -20
    assert p.avgbuyprice() == 0
    assert p.avgsellprice() == 15
    p.add(10, 20)
    assert p.quantity() == -10
    assert p.avgbuyprice() == 20
    assert p.avgsellprice() == 15
    p.add(10, 10)
    assert p.quantity() == 0
    assert p.avgbuyprice() == 15
    assert p.avgsellprice() == 15


def test_pl():
    p = Position()
    assert p.pl(10) == 0
    assert p.pl(20) == 0
    assert p.pl(5) == 0
    p.add(-10, 10)
    assert p.quantity() == -10
    assert p.avgbuyprice() == 0
    assert p.avgsellprice() == 10
    assert p.pl(10) == 0
    assert p.pl(20) == -100
    assert p.pl(5) == 50
    p.add(-10, 20)
    assert p.quantity() == -20
    assert p.avgbuyprice() == 0
    assert p.avgsellprice() == 15
    assert p.pl(10) == 100
    assert p.pl(20) == -100
    assert p.pl(5) == 200


def test_exit():
    p = Position()
    p.add(-10, 10)
    assert p.quantity() == -10
    assert p.avgbuyprice() == 0
    assert p.avgsellprice() == 10
    p.exit(20)
    assert p.quantity() == 0
    assert p.avgbuyprice() == 20
    assert p.avgsellprice() == 10
