from setuptools import setup, find_packages

with open('README.md', 'r') as f:
    longDescription = f.read()

setup(
    name='dorktrader',
    version='0.0.1',
    author='Utkarsh Tripathi',
    author_email='tripathiu7596@gmail.com',
    description='A prototype for algo-trading platform',
    long_description=longDescription,
    long_description_content_type='text/markdown',
    url='https://www.gitlab.com/tripathiu',
    project_urls={
        'Issues': 'https://gitlab.com/tripathiu/dtrader/issues'
    },
    # empty package name represents root of the project
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    install_requires=[
        'smartapi-python',
        'websocket-client',
    ]
)
