import time
from dorktrader import Instrument, AngelBroking
import argparse
import sys
# from dorktrader.Broker.BrokerAngelBrokingSim import AngelBroking

parser = argparse.ArgumentParser()
parser.add_argument("expiry")
parser.add_argument("strike", type=int)
parser.add_argument("type")
parser.add_argument("-q", type=int)
parser.add_argument("-t", type=int)
parser.add_argument("--target", type=int)
args = parser.parse_args()
expiry = args.expiry.upper()
strikePrice = args.strike
optionKind = args.type.upper()
qtty = args.q
trail = args.t
target = args.target if args.target else 0
targetPrice = None

print(
    f"\n\
--- Trailing Stoploss Strategy ---\n\
   instrument = {strikePrice} {optionKind}\n\
   {expiry = }\n\
   quantity = {qtty}\n\
   trailing sl = {trail}\n\
   {target = }\n\
----------------------------------"
)

if input("Is this okay? ").lower() not in "y yes".split():
    sys.exit()

niftyOtmCe = Instrument.Option("NIFTY", expiry, strikePrice, optionKind)
brok = AngelBroking()


class State:
    def __init__(self):
        self.i = 0

    def __next__(self):
        raise NotImplementedError("Please implement the step function for this state")


class EnterTrade(State):
    def __init__(self):
        priceNow = brok.getQuote(niftyOtmCe)
        print(f"{niftyOtmCe.symbol}: {priceNow=}")
        self.sellOrder = brok.sell(
            inst=niftyOtmCe,
            qtty=qtty,
            price=priceNow - 0.5,
        )
        global targetPrice
        targetPrice = 0 if target == 0 else (priceNow - target)
        self.orderStatus = "open"
        self.i = 10

    def step(self):
        self.orderStatus = brok.getOrderStatus(self.sellOrder).lower()
        print(f"{self.orderStatus=}")

    def __next__(self):
        if self.orderStatus == "complete":
            print(f"Order completed, moving to place SL order")
            return PlaceSlOrder(self.sellOrder)
        elif self.orderStatus == "open" and self.i > 0:  ## check this placed
            self.i -= 1
            print(f"order not executed yet... tickings: {self.i}")
            return self
        elif self.orderStatus == "rejected":  ## check this placed
            self.i -= 1
            print(f"order rejected")
            return None
        else:
            print("order was simply not executed, trying to cancel")
            brok.cancelOrder(self.sellOrder)
            time.sleep(1)
            status = brok.getOrderStatus(self.sellOrder).lower()
            if status == "cancelled":
                print("order canceled")
                return None
            elif status == "complete":
                print("order was suddenly executed!, placing SL")
                return PlaceSlOrder(self.sellOrder)


class PlaceSlOrder(State):
    def __init__(self, sellOrder):
        self.sellOrder = sellOrder
        self.trail = trail
        self.slPrice = round(float(self.sellOrder["price"]), 2) + self.trail
        print(f"{self.slPrice=}")
        self.buySlOrder = brok.buySL(
            niftyOtmCe,
            qtty=qtty,
            trigger=self.slPrice,
            price=self.slPrice + 5,
        )

    def step(self):
        print(f"SL order {brok.getOrderStatus(self.buySlOrder)}")

    def __next__(self):
        return TrailStoploss(self.sellOrder, self.buySlOrder)


class TrailStoploss(State):
    def __init__(self, sellOrder, buySlOrder):
        self.sellOrder = sellOrder
        self.buySlOrder = buySlOrder
        self.price = int(sellOrder["price"])
        self.sl = int(buySlOrder["triggerprice"])
        self.trail = self.sl - self.price
        self.nextThreshold = self.price - self.trail
        self.stop = False

    def step(self):
        self.price = brok.getQuote(niftyOtmCe)
        global targetPrice
        print(f"current asset {self.price=}, {self.sl=} {self.nextThreshold=}, {targetPrice=}")
        if brok.getOrderStatus(self.buySlOrder) == "complete":
            print("stoploss order executed, exiting")
            self.stop = True

        elif self.price < targetPrice:
            # do not exit trade, just set the sl really close to the current price and continue trailing
            print(f"TARGET reached, will keep trailing by ₹1")
            self.sl = self.price + 1
            self.nextThreshold = self.price - 1
            targetPrice = 0
            self.trail = 1


        elif self.price < self.nextThreshold:
            self.sl = self.price + self.trail
            print(f"trailing ;) new stoploss {self.sl=}")
            brok.modifyOrder(self.buySlOrder, "STOPLOSS_LIMIT", self.sl + 5, self.sl)
            self.trail = round(self.trail * 3 / 4)
            self.nextThreshold = self.nextThreshold - self.trail

    def __next__(self):
        if self.stop:
            return None
        else:
            return self


class TSL:
    def __init__(self, steptime):
        self.state = EnterTrade()
        self.steptime = steptime

    def __iter__(self):
        return self

    def __next__(self):
        self.state = next(self.state)
        if self.state == None:
            print("End of state machine")
            raise StopIteration
        time.sleep(self.steptime)
        return self


try:
    for tslstate in TSL(5):
        tslstate.state.step()
except KeyboardInterrupt:
    print(" -- exiting due to user interrupt -- ")

brok.terminate()
