import time

class State:
    def __init__(self):
        self.i = 0;

    def __next__(self):
        raise NotImplementedError("Please implement the step function for this state")

class Red(State):
    def __init__(self, i):
        self.i = i

    def step(self):
        print(f'Red: {self.i}')

    def __next__(self):
        if self.i > 0:
            self.i -= 1
            return self
        else:
            return Green(4)

class Yellow(State):
    def __init__(self, i):
        self.i = i 

    def step(self):
        print(f'Yellow: {self.i}')

    def __next__(self):
        if self.i > 0:
            self.i -= 1
            return self
        else:
            return Red(10)

class Green(State):
    def __init__(self, i):
        self.i = i 

    def step(self):
        print(f'Green: {self.i}')

    def __next__(self):
        if self.i > 0:
            self.i -= 1
            return self
        else:
            return Yellow(2)

class TrafficLights:
    def __init__(self, init=Green(4)):
        self.state = init

    def __iter__(self):
        return self

    def __next__(self):
        self.state = next(self.state)
        time.sleep(1)
        return self

try:
    for light in TrafficLights():
        light.state.step()
except KeyboardInterrupt:
    print(" -- exiting due to user interrupt -- ")

